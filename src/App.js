import React from 'react';
import Card from './Card';
import Input from './SearchInput';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      search: '',
    }
    this.onSearch = this.onSearch.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  onSearch(value) {
    this.setState({
      search: value
    })
  };

  onClick() {
    fetch(`https://api.tvmaze.com/search/shows?q=${this.state.search}`)
      .then(response => response.json())
      .then(response => this.setState({ list: response }))
  };

  render() {
    return (
      <div>
        <Input onChange={this.onSearch} onClick={this.onClick} />
        {this.state.list.map(({ show: { image, name, summary } }) => <Card src={image.original} summaryInfo={summary}
                                                                           name={name} />)}
      </div>
    )
  };
}