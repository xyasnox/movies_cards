import React from 'react';
import styled from 'styled-components';

const CardBody = styled.div`
  background-color: white;
  display: inline-block;
  width: 260px;
  border: 2px solid green;
  margin-right: 10px;
  margin-bottom: 10px;
  text-align: center;
  position: relative;
  float: left;

  &:hover {
    border: 2px solid blue;
  }
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  padding: 0;
  margin: 0;
`;

const Summary = styled.p`
  font-size: 15px;
  background-color: green;
  color: white;
  padding: 0;
  margin: 0;

  &:hover {
    background-color: blue;
  }
`;

const Name = styled.h1`
  background-color: green;
  color: white;
  padding: 0;
  margin: 0;
  display: block;

  &:hover {
    background-color: blue;
  }
`;

function createMarkup(htmlText) {
  return { __html: htmlText };
};

const Card = ({
                src,
                summaryInfo,
                name,
              }) =>
  <CardBody>
    <Name>{name}</Name>
    <Img src={src} />
    <Summary dangerouslySetInnerHTML={createMarkup(summaryInfo)} />
  </CardBody>;

export default Card;