import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
  color: palevioletred;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  background-color: white;
`;

const SearchInput = styled.input`
  background-color: green;
  color: white;
  padding: 0;
  margin: 0;
  display: block;

  &:hover {
    background-color: blue;
  }
`;

const SearchBox = styled.div`
  padding: 0;
  margin: 0;
  display: inline-block;
`;


const Input = ({
                 onClick,
                 onChange
               }) =>
  <SearchBox>
    <SearchInput onChange={(e) => onChange(e.target.value)} type='text' />
    <Button onClick={() => onClick()}> Ok, find </Button>
  </SearchBox>;

export default Input;